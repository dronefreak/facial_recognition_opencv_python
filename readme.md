#Python-OpenCV based Facial Recognition Library#

Facial recognition libraries are not available easily online, so I tried doing something on my own-

* The first step is to find a good **database of faces** with multiple images for each induvidual.

* The next step is to **detect faces** in the database images and use them to train the face recognizer.

* The last step is to **test the face recognizer** to recognize faces it was trained for. Achieved accuracy of upto 50%.

###Setup###

The database is already included in the repository. Before running the code make sure you have the following python libraries installed and running:-

* numpy - ```pip install numpy```
* imutils - ```pip install imutils```
* opencv 
* opencv-contrib

For installing OpenCV on Windows follow the [documentation here](http://docs.opencv.org/3.2.0/d5/de5/tutorial_py_setup_in_windows.html)

For installing OpenCV on Linux follow the [documentation here](http://www.pyimagesearch.com/2015/06/22/install-opencv-3-0-and-python-2-7-on-ubuntu/)

Make sure you have the contributed libraries of opencv also installed which have the face recognizer functions in-built.

Once the libraries are installed you are good to go !!

###Running the Library###

Clone the repository and run the `face_recognizer.py` script by giving
```
python face_recognizer.py
```

You should see a result similar to below, with just some specific names instead of Subject 01 etc :-

![](https://bytebucket.org/dronefreak/facial_recognition_opencv_python/raw/f1e7167aff5833badbf53998f28c6de73f3cf44c/facery.png)


###Using Custom Dataset###

Run the `take_images.py` script by giving
```
python take_images.py
```
This script allows you to take images of a person and store them in a folder in the exact same format as required by the code `subject01.wink` and so on.

If you take images from some other device, the `resize.py` script helps you to resize and rename the images in the desired format.

###Bugs###
The code is still buggy..(yes it still is !). Issues regarding accuracy are yet to be addressed, can be improved by taking a larger dataset per person.

###Maintainer###
This repo is created and maintained by [Saumya Kumaar](https://dronefreak.bitbucket.io/?) 