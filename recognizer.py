#!/usr/bin/python

# Import the required modules
import cv2
import os
import numpy as np
import sys
from PIL import Image

# For face detection we will use the Haar Cascade provided by OpenCV.
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)

# For face recognition we will the the LBPH Face Recognizer 
recognizer = cv2.createLBPHFaceRecognizer()

def get_images_and_labels(path):
    # Append all the absolute image paths in a list image_paths
    # We will not read the image with the .sad extension in the training set
    # Rather, we will use them to test our accuracy of the training
    image_paths = [os.path.join(path, f) for f in os.listdir(path) if not f.endswith('.sad')]
    # images will contains face images
    images = []
    # labels will contains the label that is assigned to the image
    labels = []
    for image_path in image_paths:
        # Read the image and convert to grayscale
        image_pil = Image.open(image_path).convert('L')
        # Convert the image format into numpy array
        image = np.array(image_pil, 'uint8')
        # Get the label of the image
        nbr = int(os.path.split(image_path)[1].split(".")[0].replace("subject", ""))
        # Detect the face in the image
        faces = faceCascade.detectMultiScale(image)
        # If face is detected, append the face to images and the label to labels
        for (x, y, w, h) in faces:
            images.append(image[y: y + h, x: x + w])
            labels.append(nbr)
            cv2.imshow("Adding faces to traning set...", image[y: y + h, x: x + w])
            cv2.waitKey(50)
    # return the images list and labels list
    return images, labels

# Path to the Yale Dataset
path = './cam_pics_test'
path_test = '/home/haraami/test_facial_recognition'
# Call the get_images_and_labels function and get the face images and the 
# corresponding labels
images, labels = get_images_and_labels(path)
cv2.destroyAllWindows()

# Perform the tranining
recognizer.train(images, np.array(labels))

image_paths = [os.path.join(path, f) for f in os.listdir(path)]

camera_port = 1
ramp_frames = 30
camera = cv2.VideoCapture(camera_port) 
def get_image():
 # read is the easiest way to get a full image out of a VideoCapture object.
    retval, im = camera.read()
    return im
 
for i in xrange(ramp_frames):
    temp = get_image()

print("Taking image...")
camera_capture = get_image()
file = "/home/haraami/test_facial_recognition/test_image.png"
cv2.imwrite(file, camera_capture)
del(camera)

print "ok now we detect"

predict_image_pil = Image.open(file).convert('L')
print "ok now we convert images to npArray"
predict_image = np.array(predict_image_pil, 'uint8')
print "ok now we detect faces"
faces = faceCascade.detectMultiScale(predict_image)
print faces
for (x, y, w, h) in faces:
    nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
    if nbr_predicted == 1:
        print "IT IS SKS....haraami saalaa"
        print conf
    elif nbr_predicted == 2:
        print "IT IS navaneeth"
        print conf
    elif nbr_predicted == 3:
        print "IT IS RAMU KAKA"
        print conf
    elif nbr_predicted == 4:
        print "IT IS AShithaaaa"
        print conf
    elif nbr_predicted == 5:
        print "It's  Gautham bitch!! Dont you know"
        print conf
    elif nbr_predicted == 6:
        print "SONALI"
        print conf
    else:
        print "could not recognize the face"

    cv2.imshow("Recognizing Face", predict_image[y: y + h, x: x + w])
    cv2.waitKey(1000)