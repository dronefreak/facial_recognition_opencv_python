import cv2

cam = cv2.VideoCapture(0)

cv2.namedWindow("test")

img_counter = 0
subject = 'subject08'
image_expression = ['.wink','.sleepy','.normal','.happy','.sad']
png = '.png'
while img_counter < 5:
    ret, frame = cam.read()
    cv2.imshow("test", frame)
    if not ret:
        break
    k = cv2.waitKey(1)

    if k%256 == 27:
        # ESC pressed...so EXIT
        print("Escape hit, closing...")
        break
    elif k%256 == 32:
        # SPACE pressed...so take next photo
        img_path = '/home/haraami/test_facial_recognition/'
        img_name = img_path+subject+image_expression[img_counter]+png
        cv2.imwrite(img_name, frame)
        print("{} written!".format(img_name))
        img_counter += 1

cam.release()
cv2.destroyAllWindows()