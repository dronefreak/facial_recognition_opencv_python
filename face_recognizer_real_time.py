# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 11:30:26 2016

@author: ullu_da_pattha
"""

import cv2
import os
import numpy as np
from PIL import Image
import sys

# For face detection we will use the Haar Cascade provided by OpenCV.
cascadePath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)

cam = cv2.VideoCapture(0)
# For face recognition we will the the LBPH Face Recognizer 
recognizer = cv2.createLBPHFaceRecognizer()

def get_images_and_labels(path):
    # Append all the absolute image paths in a list image_paths
    # We will not read the image with the .sad extension in the training set
    # Rather, we will use them to test our accuracy of the training
    image_paths = [os.path.join(path, f) for f in os.listdir(path) if not f.endswith('.sad')]
    # images will contains face images
    images = []
    # labels will contains the label that is assigned to the image
    labels = []
    for image_path in image_paths:
        # Read the image and convert to grayscale
        image_pil = Image.open(image_path).convert('L')
        # Convert the image format into numpy array
        image = np.array(image_pil, 'uint8')
        # Get the label of the image
        nbr = int(os.path.split(image_path)[1].split(".")[0].replace("subject", ""))
        # Detect the face in the image
        faces = faceCascade.detectMultiScale(image)
        # If face is detected, append the face to images and the label to labels
        for (x, y, w, h) in faces:
            images.append(image[y: y + h, x: x + w])
            labels.append(nbr)
            cv2.imshow("Adding faces to traning set...", image[y: y + h, x: x + w])
            cv2.waitKey(50)
    # return the images list and labels list
    return images, labels

# Path to the Yale Dataset
path = './cam_pics_test'
# Call the get_images_and_labels function and get the face images and the 
# corresponding labels
images, labels = get_images_and_labels(path)
cv2.destroyAllWindows()

# Perform the tranining
recognizer.train(images, np.array(labels))
while True:
    # Capture frame-by-frame
    ret, frame = cam.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    predict_image = np.array(gray, 'uint8')
    faces = faceCascade.detectMultiScale(
        predict_image,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE
    )

    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        nbr_predicted, conf = recognizer.predict(predict_image[y: y + h, x: x + w])
        if nbr_predicted == 1:
            print "IT IS SKS....haraami saalaa"
        elif nbr_predicted == 2:
            print "IT IS navaneeth"
        elif nbr_predicted == 3:
            print "IT IS RAMU KAKA"
        elif nbr_predicted == 4:
            print "IT IS AShithaaaa"
        elif nbr_predicted == 5:
            print "It's  Gautham bitch!! Dont you know"
        elif nbr_predicted == 6:
		print "SONALI"
        
    cv2.imshow('Video', frame)
#    stereo = cv2.createStereoBM(numDisparities=16, blockSize=15)
#    disparity = stereo.compute(imgL,imgR)
#    plt.imshow(disparity,'gray')

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
cam.release()
cv2.destroyAllWindows()
