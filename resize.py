#!/usr/bin/python
from PIL import Image
import os, sys
import numpy as np

path = "/home/haraami/Downloads/attachments/"
path_for_saving = "/home/haraami/test_facial_recognition"
dirs = os.listdir( path )

def resize():
    for item in dirs:
        if os.path.isfile(path+item):
            im = Image.open(path+item)
            f, e = os.path.splitext(path_for_saving+item)
            imResize = im.resize((223,324), Image.ANTIALIAS)
            imResize.save(f + ' resized.jpg', 'JPEG', quality=90)

resize()